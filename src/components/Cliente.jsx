import React from 'react'
import { Form, redirect, useNavigate } from 'react-router-dom'
import { eliminarCliente } from '../api/clientes'

export async function action({params}){
    await eliminarCliente(params.clienteId)
    return redirect('/')
}

const Cliente = ({data}) => {

    const navigate = useNavigate()
    return (
        <tr className='border-b'>
            <td className="p-6 space-y-2">
                <p className="text-2xl text-gray-800">{data.nombre}</p>
                <p>{data.empresa}</p>
            </td>
            <td className="p-6">
                <p className="text-gray-600">
                    <span className="text-gray-800 uppercase font-bold">Email: </span>
                    {data.email}
                </p>
                <p className="text-gray-600">
                    <span className="text-gray-800 uppercase font-bold">Tel: </span>
                    {data.telefono}
                </p>
            </td>
            <td className="p-6 flex gap-10">
                <button 
                    className="text-purple-600 hover:text-purple-700 upeercase font-bold text-xs"
                    onClick={() =>  navigate(`/clientes/${data.id}/editar`)}
                >
                    Editar
                </button>
                <Form
                    method='post'
                    action={`/clientes/${data.id}/eliminar`}
                    onSubmit={(e) => {
                        if(!confirm('Deseaas eliminar este registro??')){
                            e.preventDefault()
                        }
                    }}
                >
                    <button 
                        className="text-red-600 hover:text-red-700 upeercase font-bold text-xs"
                        type='submit'
                    >
                        Eliminar
                    </button>
                </Form>
                
            </td>
        </tr>
    )
}

export default Cliente
