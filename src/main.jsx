import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import NuevoCliente, { action as nuevoClienteAction} from './pages/NuevoCliente'
import Index, { loader as clienteLoader} from './pages/Index'
import EdtarCliente, { loader as editCLientLoader, action as editarClienteAction}  from './pages/EdtarCliente'
import { action as deleteClientAction} from './components/Cliente'

import Layout from './components/Layout'
import ErrorPage from './components/ErrorPage'

const router = createBrowserRouter([
  {
    path:'/',
    element: <Layout />,
    children:[
      {
        index: true,
        element: <Index />,
        loader: clienteLoader,
        errorElement:<ErrorPage/>
      },
      {
        path:'/clientes/nuevo',
        element: <NuevoCliente />,
        action: nuevoClienteAction
      },
      {
        path:'/clientes/:clienteId/editar',
        element: <EdtarCliente />,
        loader: editCLientLoader,
        errorElement: <ErrorPage />,
        action: editarClienteAction
      },
      {
        path:'/clientes/:clienteId/eliminar',
        action: deleteClientAction
      }
    ]
  },
  
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>,
)
