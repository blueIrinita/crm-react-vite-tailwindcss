import { useNavigate, Form, useLoaderData, useActionData, redirect } from "react-router-dom"
import { agregarCliente } from "../api/clientes"
import Error from "../components/Error"
import Formulario from "../components/Formulario"

export async function action({request}) {
    const formData = await request.formData()
    const datos = Object.fromEntries(formData)

    const email = formData.get('email')

    const errores = []
    if(Object.values(datos).includes('')){
        errores.push('TOdos los datos son obligatorios')
    }

    let regex = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
    if(!regex.test(email)){
        errores.push('El correo no es valido')
    }

    if(Object.keys(errores).length){
        return errores
    }

    await agregarCliente(datos)

    return redirect('/')

}

const NuevoCliente = () => {

    const errores = useActionData()
    const navigate = useNavigate()

  return (
    <>
        <h1 className="font-black text-4xl text-pink-900">Clientes</h1>
        <p className="mt-3">
            Administra tus clientes
        </p>
        <div className="flex justify-end">
            <button 
                className="bg-pink-800 text-white px-3 py-1 font-bold uppercase"
                onClick={() => navigate(-1)}
            >
                Volver
            </button>
        </div>
        <div className="bg-white shadow rounded-md md:w-3/4 mx-auto px-5 py-10 mt-20">
            {
                errores?.length && errores.map((error, index) => <Error key={index}>{error}</Error>)
            }
            <Form method="POST" noValidate >
                <Formulario />
                <input 
                    type="submit" 
                    className="mt-5 w-full bg-pink-800 p-3 uppercase font-bold test-white text-lg rounded-md text-white" 
                    value={"Registrar Cliente"}
                />
            </Form>
        </div>
    </>
  )
}

export default NuevoCliente
